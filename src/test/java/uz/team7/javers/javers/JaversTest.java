package uz.team7.javers.javers;

import org.javers.core.Changes;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.NewObject;
import org.javers.core.diff.changetype.ValueChange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uz.team7.javers.person.Gender;
import uz.team7.javers.person.Person;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JaversTest {

    private static final Javers javers = JaversBuilder.javers().build();

    private static final UUID ID = UUID.fromString("189bdc78-a23d-461d-8922-a7107dc61c5f");

    private static final String NAME_V1 = "Mohammed";
    private static final Integer AGE_V1 = 20;
    private static final Gender GENDER_V1 = Gender.MALE;
    private static final UUID FATHER_ID_V1 = UUID.fromString("9d3fe399-1441-45be-bcfd-50e9613ed398");

    private static final String NAME_V2 = "Abdullah";
    private static final Integer AGE_V2 = 21;
    private static final Gender GENDER_V2 = Gender.FEMALE;
    private static final UUID FATHER_ID_V2 = UUID.fromString("41356cd0-ef91-4dc2-89fc-62482fa52920");

    private static Person person_1;
    private static Person person_2;

    @BeforeEach
    void init() {
        person_1 = getPerson(1);
        person_2 = getPerson(2);
    }

    @Test
    void testDiff() {
        Diff diff = javers.compare(person_1, person_2);
        Changes changes = diff.getChanges();

        assertEquals(8, changes.size());

        assertEquals(1, changes.getChangesByType(NewObject.class).size());

        List<ValueChange> typeChanges = changes.getChangesByType(ValueChange.class);
        assertEquals(3, typeChanges.size());
    }

    private Person getPerson(int choice) {
        return switch (choice) {
            case 1 -> new Person()
                    .setId(ID)
                    .setName(NAME_V1)
                    .setAge(AGE_V1)
                    .setGender(GENDER_V1)
                    .setFather(new Person()
                            .setId(FATHER_ID_V1)
                    );
            case 2 -> new Person()
                    .setId(ID)
                    .setName(NAME_V2)
                    .setAge(AGE_V2)
                    .setGender(GENDER_V2)
                    .setFather(new Person()
                            .setId(FATHER_ID_V2)
                    );
            default -> null;
        };
    }

}
