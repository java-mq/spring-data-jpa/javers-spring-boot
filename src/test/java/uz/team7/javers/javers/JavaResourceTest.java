package uz.team7.javers.javers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import uz.team7.javers.person.Gender;
import uz.team7.javers.person.Person;
import uz.team7.javers.person.PersonRepository;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class JavaResourceTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private JaversResource javersResource;

    private static UUID ID;

    private static final String NAME_V1 = "Mohammed";
    private static final Integer AGE_V1 = 20;
    private static final Gender GENDER_V1 = Gender.MALE;

    private static final String NAME_V2 = "Abdullah";
    private static final Integer AGE_V2 = 21;
    private static final Gender GENDER_V2 = Gender.FEMALE;

    private static Person person;

    @BeforeEach
    void init() {
        person = getPerson();
    }

    @Test
    @Transactional
    void testGetAuditedEntities() {
        List<String> auditedEntities = javersResource.getAuditedEntities();

        assertEquals(1, auditedEntities.size());
        assertThat(auditedEntities.getFirst()).isEqualTo(Person.class.getName());
    }

    @Test
    @Transactional
    void testGetChangesWhitOnlySavedData() throws ClassNotFoundException {
        personRepository.save(person);
        ID = person.getId();

        List<EntityAuditEventDTO> changes = javersResource.getChanges(Person.class.getName(), 100).getBody();

        assertThat(changes).isNotEmpty();
        assertThat(changes.size()).isEqualTo(1);


    }

    @Test
    @Transactional
    void testGetChangesWhitChanges() throws ClassNotFoundException {
        personRepository.save(person);
        ID = person.getId();

        person.setName(NAME_V2);
        person.setAge(AGE_V2);
        person.setGender(GENDER_V2);
        personRepository.save(person);

        List<EntityAuditEventDTO> changes = javersResource.getChanges(Person.class.getName(), 100).getBody();

        assertThat(changes).isNotEmpty();
        assertThat(changes.size()).isEqualTo(2);

        EntityAuditEventDTO initialSnapshot = changes.get(1);
        assertThat(initialSnapshot.getEntityId()).isEqualTo(ID.toString());
        assertThat(initialSnapshot.getEntityType()).isEqualTo(Person.class.getName());
        assertThat(initialSnapshot.getAction()).isEqualTo("INITIAL");
        assertThat(initialSnapshot.getCommitVersion()).isEqualTo(1);

        EntityAuditEventDTO fieldChanges = changes.getFirst();
        assertThat(fieldChanges.getEntityId()).isEqualTo(ID.toString());
        assertThat(fieldChanges.getEntityType()).isEqualTo(Person.class.getName());
        assertThat(fieldChanges.getAction()).isEqualTo("UPDATE");
        assertThat(fieldChanges.getCommitVersion()).isEqualTo(2);
    }

    @Test
    @Transactional
    void testGetChangesWhitDeletion() throws ClassNotFoundException {
        personRepository.save(person);
        ID = person.getId();

        personRepository.delete(person);

        List<EntityAuditEventDTO> changes = javersResource.getChanges(Person.class.getName(), 100).getBody();

        assertThat(changes).isNotEmpty();
        assertThat(changes.size()).isEqualTo(2);

        EntityAuditEventDTO initialSnapshot = changes.get(1);
        assertThat(initialSnapshot.getEntityId()).isEqualTo(ID.toString());
        assertThat(initialSnapshot.getEntityType()).isEqualTo(Person.class.getName());
        assertThat(initialSnapshot.getAction()).isEqualTo("INITIAL");
        assertThat(initialSnapshot.getCommitVersion()).isEqualTo(1);

        EntityAuditEventDTO fieldChanges = changes.getFirst();
        assertThat(fieldChanges.getEntityId()).isEqualTo(ID.toString());
        assertThat(fieldChanges.getEntityType()).isEqualTo(Person.class.getName());
        assertThat(fieldChanges.getAction()).isEqualTo("TERMINAL");
        assertThat(fieldChanges.getCommitVersion()).isEqualTo(2);
    }

    private Person getPerson() {
        return new Person()
                .setName(NAME_V1)
                .setAge(AGE_V1)
                .setGender(GENDER_V1);
    }

}
