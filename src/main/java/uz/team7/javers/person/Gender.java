package uz.team7.javers.person;


/**
 * There are only two genders
 */
public enum Gender {
    MALE,
    FEMALE
}
