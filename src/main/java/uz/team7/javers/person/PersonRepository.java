package uz.team7.javers.person;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@JaversSpringDataAuditable
public interface PersonRepository extends JpaRepository<Person, UUID> {
}
