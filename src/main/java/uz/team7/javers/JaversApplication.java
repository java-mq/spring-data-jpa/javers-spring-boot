package uz.team7.javers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaversApplication {

    public static void main(String[] args) {
        SpringApplication.run(JaversApplication.class, args);
    }

}
