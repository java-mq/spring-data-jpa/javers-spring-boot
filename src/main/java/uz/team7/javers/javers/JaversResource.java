package uz.team7.javers.javers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javers.core.Javers;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.QueryBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.team7.javers.person.Person;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class JaversResource {

    private final Javers javers;

    @GetMapping("/audits/entity/all")
    public List<String> getAuditedEntities() {
        return List.of(
                Person.class.getName()
        );
    }

    @GetMapping("/audits/entity/changes")
    public ResponseEntity<List<EntityAuditEventDTO>> getChanges(
            @RequestParam(value = "entityType") String entityType,
            @RequestParam(value = "limit") int limit
    ) throws ClassNotFoundException {
        log.debug("REST request to get a page of EntityAuditEventDTOs");

        return ResponseEntity.ok(javers
                .findSnapshots(
                        QueryBuilder.byClass(
                                        Class.forName(entityType)
                                )
                                .limit(limit)
                                .build()
                )
                .stream()
                .map(snapshot -> EntityAuditEventDTO.fromJaversSnapshot(snapshot).setEntityType(entityType))
                .toList());
    }

    @GetMapping("/audits/entity/changes/version/previous")
    public ResponseEntity<EntityAuditEventDTO> getPrevVersion(
            @RequestParam(value = "qualifiedName") String qualifiedName,
            @RequestParam(value = "entityId") String entityId,
            @RequestParam(value = "commitVersion") Long commitVersion
    ) throws ClassNotFoundException {
        log.debug("REST request to get previous version of EntityAuditEventDTOs");

        List<CdoSnapshot> snapshots = javers
                .findSnapshots(
                        QueryBuilder
                                .byInstanceId(entityId, Class.forName(qualifiedName))
                                .limit(1)
                                .withVersion(commitVersion - 1).build()
                );

        if (snapshots == null || snapshots.isEmpty()) {
            return ResponseEntity.ok(new EntityAuditEventDTO());
        }

        return new ResponseEntity<>(EntityAuditEventDTO.fromJaversSnapshot(snapshots.getFirst()), HttpStatus.OK);
    }

    @GetMapping("/audits/entity/history")
    public ResponseEntity<List<CdoSnapshot>> getHistoryByEntityId(@RequestParam(value = "qualifiedName") String qualifiedName,
                                                                  @RequestParam(value = "entityId") String entityId
    ) throws ClassNotFoundException {
        log.info("Rest request to get history from entity: {}, and id: {}", qualifiedName, entityId);

        return ResponseEntity.ok(
                javers
                        .findSnapshots(
                                QueryBuilder
                                        .byInstanceId(entityId, Class.forName(qualifiedName))
                                        .build()
                        )
        );
    }

}
