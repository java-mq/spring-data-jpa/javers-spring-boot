package uz.team7.javers.javers;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.javers.core.metamodel.object.CdoSnapshot;

import java.time.Instant;
import java.time.ZoneOffset;

@Setter
@Getter
@Accessors(chain = true)
public class EntityAuditEventDTO {

    private String entityId;

    private String entityType;

    private String action;

    private String entityValue;

    private Integer commitVersion;

    private String modifiedBy;

    private Instant modifiedDate;

    public static EntityAuditEventDTO fromJaversSnapshot(@NonNull CdoSnapshot snapshot) {
        EntityAuditEventDTO entityAuditEventDTO = new EntityAuditEventDTO()
                .setAction(snapshot.getType().name())
                .setCommitVersion(Long.valueOf(snapshot.getVersion()).intValue())
                .setEntityType(snapshot.getManagedType().getName())
                .setEntityId(snapshot.getGlobalId().value().split("/")[1])
                .setModifiedBy(snapshot.getCommitMetadata().getAuthor())
                .setModifiedDate(snapshot.getCommitMetadata().getCommitDate().toInstant(ZoneOffset.UTC));

        if (!snapshot.getState().getPropertyNames().isEmpty()) {
            int count = 0;
            StringBuilder sb = new StringBuilder("{");

            for (String s : snapshot.getState().getPropertyNames()) {
                count++;
                Object propertyValue = snapshot.getPropertyValue(s);
                sb
                        .append("\"")
                        .append(s)
                        .append("\": \"")
                        .append(propertyValue)
                        .append("\"");
                if (count < snapshot.getState().getPropertyNames().size()) {
                    sb.append(",");
                }
            }

            sb.append("}");
            entityAuditEventDTO.setEntityValue(sb.toString());
        }

        return entityAuditEventDTO;
    }

}
